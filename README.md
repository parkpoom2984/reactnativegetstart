This project create for get starter react native with typescript example demo using

[* Redux](https://github.com/reactjs/redux)

[* Redux Saga](https://github.com/redux-saga/redux-saga)

[* Apisauce](https://github.com/skellock/apisauce)

[* Reduxsauce](https://www.npmjs.com/package/reduxsauce)

[* Reactotron](https://github.com/infinitered/reactotron)

## Table of Contents

* [How to build project](#how-to-build-project)

## How to build project

download / clone porject

`npm i`

`npm run build`

MacOS

`npm run ios`

Android

`npm run android`