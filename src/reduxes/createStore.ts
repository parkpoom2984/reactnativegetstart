import { applyMiddleware } from 'redux'
import rootReducer from '../reduxes'
import Reactotron from 'reactotron-react-native'
import createSagaMiddleware from 'redux-saga'
import { dataSaga } from '../sagas'

const sagaMiddleware = createSagaMiddleware()

export default function configureStore() {
  //const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
  const store = Reactotron.createStore(rootReducer, applyMiddleware(sagaMiddleware))
  sagaMiddleware.run(dataSaga)
  return store
}