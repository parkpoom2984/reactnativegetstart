export interface TestAction {
    fetchingData()
    fetchingDataSuccess(name: string)
    fetchingDataFailure()
}

export interface TestCreateAction {
    fetchingData: null
    fetchingDataSuccess: any
    fetchingDataFailure: null
}

export interface TestType {
    FETCHING_DATA
    FETCHING_DATA_SUCCESS
    FETCHING_DATA_FAILURE
}

export interface TestState {
    name: string
    dataFetched: boolean
    isFetching: boolean
    error: boolean
}