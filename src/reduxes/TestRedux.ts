import { createReducer, createActions } from 'reduxsauce'
import { TestCreateAction, TestAction, TestType, TestState } from './TestRedux.d'

/* ------------- Types and Action Creators ------------- */

const Actions: TestCreateAction = {
    fetchingData: null,
    fetchingDataSuccess: ['name'],
    fetchingDataFailure: null
}

const { Types, Creators } = createActions(Actions)
export const TestTypes: TestType = Types

export const TestActions: TestAction = Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
    name: '',
    dataFetched: false,
    isFetching: false,
    error: false
}

/* ------------- Reducers ------------- */

export const fetchingData = (state: TestState) => {
    return {
        ...state,
        name: '',
        isFetching: true
    }
}

export const fetchingDataSuccess = (state: TestState, action) => {
    return {
        ...state,
        isFetching: false,
        name: action.name
    }
}

export const fetchingDataFailure = (state: TestState) => {
    return {
        ...state,
        isFetching: false,
        error: true
    }
}

/* ------------- Hookup Reducers To Types ------------- */

export const TestReducer = createReducer(INITIAL_STATE, {
    [TestTypes.FETCHING_DATA]: fetchingData,
    [TestTypes.FETCHING_DATA_SUCCESS]: fetchingDataSuccess,
    [TestTypes.FETCHING_DATA_FAILURE]: fetchingDataFailure,
})
