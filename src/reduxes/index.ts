import { combineReducers } from 'redux'
import {TestReducer} from './TestRedux'

const rootReducer = combineReducers({
    test: TestReducer
})

export default rootReducer