import './config/reactotron'
import React, { Component } from 'react'
import { NavigationRouter } from './navigation/NavigationRouter'
import { Provider } from 'react-redux'
import createStore from '../src/reduxes/CreateStore'
export class Index extends Component<any, any>{
    render() {
        const store = createStore()
        return (
            <Provider store={store}>
                <NavigationRouter />
            </Provider>
        )
    }
}