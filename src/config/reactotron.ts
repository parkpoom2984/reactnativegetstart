const Reactotron = require('reactotron-react-native').default
const apisaucePlugin = require('reactotron-apisauce')
const { reactotronRedux } = require('reactotron-redux')
const sagaPlugin = require('reactotron-redux-saga')

Reactotron
    .configure({
      // host: 'localhost', // default is localhost (on android don't forget to `adb reverse tcp:9090 tcp:9090`)
      name: 'ReactNativeGetStart', // would you like to see your app's name?
      // server: 'localhost', // IP of the server to connect to
      // port: 3334, // Port of the server to connect to (default: 3334)
      // enabled: true // Whether or not Reactotron should be enabled.
    })

    // register apisauce so we can install a monitor later
    .use(apisaucePlugin())

    // setup the redux integration with Reactotron
    .use(reactotronRedux())

    // register the redux-saga plugin so we can use the monitor in CreateStore.js
    .use(sagaPlugin())

    // let's connect!
    .connect()

  // Let's clear Reactotron on every time we load the app
  Reactotron.clear()

  // Totally hacky, but this allows you to not both importing reactotron-react-native
  // on every file.  This is just DEV mode, so no big deal.
  //console.tron = Reactotron