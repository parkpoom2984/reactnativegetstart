import {TestActions} from '../reduxes/TestRedux'
import { put, call } from 'redux-saga/effects'
import testApi from '../api/test'
import { pickAll } from 'ramda'

export function* fetchData(action) {
  try {
    const response = yield call(testApi.testRequest)
    const pickData = pickAll(['data'], response)
    const name = pickData.data.name
    yield put(TestActions.fetchingDataSuccess(name))
  } catch (e) {
    yield put(TestActions.fetchingDataFailure())
  }
}

