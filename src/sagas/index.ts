import {TestTypes} from '../reduxes/TestRedux'
import { takeEvery, all } from 'redux-saga/effects'
import { fetchData } from './TestSagas'

export function* dataSaga() {
  yield all(
    [
      takeEvery(TestTypes.FETCHING_DATA, fetchData)
    ]
  )
}