const KEY = {
  content_type: 'Content-Type',
}

const VALUE = {
  json: 'application/json',
}

export const Headers = {
    default : {
        [KEY.content_type] : VALUE.json
    }
}

export default Headers