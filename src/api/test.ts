import api from './api'
import {TestAPI} from './test.d'

const testApi : TestAPI = {
    testRequest: () => api.get('/users/parkpoom2984')
}

export default testApi