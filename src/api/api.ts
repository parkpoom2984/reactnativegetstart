import {Headers} from '../common/headers'
import {create} from 'apisauce'
import Reactotron from 'reactotron-react-native'
const api = create({
  baseURL: 'https://api.github.com',
  headers : Headers.default
})

api.addMonitor(Reactotron.apisauce)

export default api