import React, { Component } from 'react'
import { Router, Scene } from 'react-native-router-flux'
import { Home, Splash } from '../../containers/Home'
import { NavItems } from '../../navigation/NavItems'
export class NavigationRouter extends Component<any, any> {
    render() {
        return (
            <Router>
                <Scene key="root">
                    <Scene key="home" component={Home} title="Home" initial />
                    <Scene key="splash" component={Splash} title="Splash" renderLeftButton={NavItems.backNav} />
                </Scene>
            </Router>
        )
    }
}