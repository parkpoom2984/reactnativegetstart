import { NavActions } from '../../navigation/NavActions'
import { Image,View,TouchableHighlight } from 'react-native'
import React from 'react'
const backButtonOnPress = () => NavActions.pop()

export function backNav (){
    return (
        <View>
            <TouchableHighlight onPress={ () => backButtonOnPress()}>
                <Image source={require('../../img/right_arrow_button.png')}/>
            </TouchableHighlight>
        </View>
    )
}

export const NavItems = {
    backNav
}

export default NavItems