import { Actions } from 'react-native-router-flux'

export function pop() {
    Actions.pop()
}

export const NavActions = {
    pop,
}

export default NavActions