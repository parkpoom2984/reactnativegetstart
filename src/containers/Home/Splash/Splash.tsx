import React, { Component } from 'react'

import {
    View,
    Text,
    StyleSheet
} from 'react-native'

export class Splash extends Component<any, any>{
    state = { name: 'test app splash' }
    render() {
        return (
            <View style={styles.container}>
                <Text>{this.state.name}</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});