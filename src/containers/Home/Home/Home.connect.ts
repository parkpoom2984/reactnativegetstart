import { ComponentClass } from 'react'
import { connect } from 'react-redux'
import { TestActions } from '../../../reduxes/TestRedux'
import { HomeProps, HomeConnectDispatchProps, HomeConnectStateProps } from './Home.d'
import { Home as View } from './Home'

const mapStateToProps = (state): HomeConnectStateProps => {
    return {
        name: state.test.name
    }
}


const mapDispatchToProps = (dispatch: Function): HomeConnectDispatchProps => {
    return {
        fetchData: () => dispatch(TestActions.fetchingData())
    }
}


export const Home = connect(mapStateToProps, mapDispatchToProps)(View) as ComponentClass<HomeProps>
export type Home = HomeProps