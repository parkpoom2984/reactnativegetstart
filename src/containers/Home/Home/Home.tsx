import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import { HomeConnectProps, HomeState } from './Home.d'
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight
} from 'react-native'

export class Home extends Component<HomeConnectProps, HomeState>{
    state = { name: 'test app', nameGithub: '' }

    _onPressSecondScreen = () => {
        Actions.splash()
    }

    _onPressFetchData = () => {
        this.props.fetchData()
    }

    componentWillReceiveProps(newProps: HomeConnectProps) {
        const { name } = newProps
        this.setState({
            nameGithub: name
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableHighlight onPress={() => this._onPressSecondScreen()}>
                    <Text>{this.state.name}</Text>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => this._onPressFetchData()}>
                    <Text>FetchData</Text>
                </TouchableHighlight>
                <Text>{this.state.nameGithub}</Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});