import { ViewProperties } from 'react-native'

export interface HomeProps extends ViewProperties {

}


export interface HomeConnectStateProps {
    name: string
}

export interface HomeConnectDispatchProps {
    fetchData: () => void
}

export interface HomeState {

}

export interface HomeConnectProps extends HomeProps, HomeConnectStateProps, HomeConnectDispatchProps {
}